var array = [];

$(function() {
	google.charts.load('current', {'packages':['corechart']});
	
	$("#showChart").click(function(){
		//var array = [];
		array = [];
		$('#grid > tbody  > tr').each(function() {
			var values = [];
			var con, abs;
			$(this).find("input").each(function(){
				if (this.type != 'button'){
					if (this.classList[0] == 'con') con = parseFloat(this.value);
					if (this.classList[0] == 'abs') abs = parseFloat(this.value);
				}
			});
			values.push(abs);	// data.addColumn('number', 'Absorvancia');
			values.push(con);	// data.addColumn('number', 'Concentración');
			values.push(null);	// data.addColumn({type:'string', role:'style'});
			array.push(values);
		});
		
		var data = new google.visualization.DataTable();
		data.addColumn('number', 'Absorvancia');
		data.addColumn('number', 'Concentración');
		data.addColumn({type:'string', role:'style'});
		data.addRows(array);
		
		google.charts.setOnLoadCallback(drawChart(data));
		
		$('#factor').show();
	})
	
	$('#findCalc').click(function(){
		var conStd = parseFloat($('#conStd').val());
		var absStd = parseFloat($('#absStd').val());
		
		var absMst = parseFloat($('#absMst').val());
		var conMst = (conStd / absStd) * absMst;
		
		$('#conMst').val(conMst.toFixed(3));
		
		var values = [];
		values.push(absMst);
		values.push(conMst);
		values.push('point { size: 14; shape-type: star; fill-color: #EF851C; shape-sides: 4 }');
		array.push(values);
		
		var data = new google.visualization.DataTable();
		data.addColumn('number', 'Absorvancia');
		data.addColumn('number', 'Concentración');
		data.addColumn({type:'string', role:'style'});
		data.addRows(array);
		
		google.charts.setOnLoadCallback(drawChart(data));
	})
	
});


function drawChart(data) {
	/*var data = google.visualization.arrayToDataTable([
		['Absrovancia', 'Concentración'],
		[8, 37], [4, 19.5], [11, 52], [4, 22], [3, 16.5], [6.5, 32.8], [14, 72]]);
	
	var data = google.visualization.arrayToDataTable([
		['Absrovancia', 'Concentración'],
		[0.10, 10], [0.20, 20], [0.30, 30], [0.40, 40], [0.50, 50]]);*/
		
	//var data = google.visualization.arrayToDataTable(dataArray);
	/*
	var options = {
		title: 'Curva de Calibración',
		hAxis: {title: 'Absorvancia'},
		vAxis: {title: 'Concentración'},
		legend: 'none',
		trendlines: { 0: {} }    // Draw a trendline for data series 0.
	};
	*/
	var options = {
		legend: 'none',
		title: 'Curva de Calibración',
		hAxis: {title: 'Absorvancia'},
		vAxis: {title: 'Concentración'},
		//colors: ['#EF851C'],
		pointSize: 10,
		//curveType: 'function',
		//pointShape: 'square'
	};

	//var chart = new google.visualization.ScatterChart(document.getElementById('chart'));
	//chart.draw(data, options);
	data.sort({column: 1, desc: false});
	var chart = new google.visualization.LineChart(document.getElementById('chart'));
	chart.draw(data, options);
}