$(function() {
	var rownumber = 1;
	var points = 0;
	
	$('#newRow').click(function(){
		//alert('Hola mundo');
		var newRow = $("<tr>");
        var cols = "";
		rownumber += 1;
		
        cols += '<th scope="row">' + rownumber + '</th>';
        cols += '<td><input type="number" class="con form-control" value="0"/></td>';
        cols += '<td> mg/dl</td>';
		cols += '<td><input type="number" class="abs form-control" value="0"/></td>';
		cols += '<td><input type="number" class="mue form-control" value="0"/></td>';
        cols += '<td><input type="button" class="ibtnDel btn btn-danger "  value="Quitar Fila"></td>';
        newRow.append(cols);
        $("#grid").append(newRow);
	})
	
	$("#grid").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
		rownumber -= 1;
    });
});