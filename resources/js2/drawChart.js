var array = [];

$(function() {
	google.charts.load('current', {'packages':['corechart']});
	
	$("#showChart").click(function(){
		array = [];
		$('#grid > tbody  > tr').each(function() {
			var values = [];
			var con, abs, mue;
			$(this).find("input").each(function(){
				if (this.type != 'button'){
					if (this.classList[0] == 'con') con = parseFloat(this.value);
					if (this.classList[0] == 'abs') abs = parseFloat(this.value);
					if (this.classList[0] == 'mue') mue = parseFloat(this.value);
				}
			});
			values.push(abs);	// data.addColumn('number', 'Absorvancia');
			values.push(con);	// data.addColumn('number', 'Concentración');
			values.push(null);	// data.addColumn({type:'string', role:'style'});
			array.push(values);
			
			if(mue > 0){
				values = []
				var conMue = (con / abs) * mue;
				values.push(mue);
				values.push(conMue);
				values.push('point { size: 14; shape-type: star; fill-color: #EF851C; shape-sides: 4 }');
				array.push(values);
			}
		});
		
		var data = new google.visualization.DataTable();
		data.addColumn('number', 'Absorvancia');
		data.addColumn('number', 'Concentración');
		data.addColumn({type:'string', role:'style'});
		data.addRows(array);
		
		google.charts.setOnLoadCallback(drawChart(data));
	})
});


function drawChart(data) {
	var options = {
		legend: 'none',
		title: 'Curva de Calibración',
		hAxis: {title: 'Absorvancia'},
		vAxis: {title: 'Concentración'},
		pointSize: 10,
	};
	
	data.sort({column: 1, desc: false});
	var chart = new google.visualization.LineChart(document.getElementById('chart'));
	chart.draw(data, options);
}